<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('all_tasks');
});

Auth::routes();

Route::group(['prefix' => 'task'], function () {
    Route::get('all', ['as' => 'all_tasks', 'uses' => 'TaskController@index']);
    Route::get('new', ['as' => 'new_task', 'uses' => 'TaskController@create']);
    Route::post('new', ['as' => 'create_task', 'uses' => 'TaskController@create']);
    Route::get('view/{id}', ['as' => 'view_task', 'uses' => 'TaskController@view'])
        ->where('id', '[0-9]+');
    Route::delete('delete/{id}', ['as' => 'delete_task', 'uses' => 'TaskController@delete'])
        ->where('id', '[0-9]+');
});