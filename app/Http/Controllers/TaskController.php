<?php

namespace App\Http\Controllers;

use App\TaskStatus;
use Illuminate\Http\Request;
use App\Task as Task;
use App\User as User;
use App\TaskStatus as Status;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('master', 'executor', 'status')->get();

        return view('tasks.index', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required|string|max:255',
                'description' => 'required|string|max:255',
                'status' => 'required|numeric',
                'executor' => 'nullable|numeric'
            ]);

            Task::create([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'status_id' => $request->input('status'),
                'master_id' => $request->user()->id,
                'executor_id' => $request->input('executor')

            ]);

            return redirect()->route('all_tasks');

        }

        $users = User::all();
        $statuses = Status::all();

        return view('tasks.create', [
            'users' => $users,
            'statuses' => $statuses
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request
     * @param  integer
     *
     * @return \Illuminate\Http\Response
     */
    public function view (Request $request, $id)
    {
        $task = Task::with('master', 'executor', 'status')->find($id);

        //TODO check if task with this ID exists

        return view('tasks.view', [
            'task' => $task,
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request
     * @param  integer
     *
     * @return \Illuminate\Http\Response
     */
    public function delete (Request $request, $id)
    {
        //TODO check if task with this ID exists

        Task::destroy($id);

        return redirect()->route('all_tasks');
    }
}



