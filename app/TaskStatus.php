<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'tasks_statuses';

   const OPEN = 1;
   const IN_PROGRESS = 2;
   const IN_REVIEW = 3;
   const CLOSED = 4;
}
