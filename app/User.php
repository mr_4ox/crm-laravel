<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all tasks which user created as "Master"
     */
    public function createdTasks()
    {
        return $this->hasMany('App\Task', 'master_id', 'id');
    }

    /**
     * Get all tasks which were assigned to user as "Executor"
    */
    public function assignedTasks()
    {
        return $this->hasMany('App\Task','executor_id', 'id');
    }
}
