<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'status_id', 'master_id', 'executor_id'
    ];

    /**
     * Get user which has created task as "Master"
     */
    public function master()
    {
        return $this->belongsTo('App\User','master_id','id');
    }

    /**
     * Get user which has been assigned to task as "Executor"
     */
    public function executor()
    {
        return $this->belongsTo('App\User', 'executor_id', 'id');
    }

    /**
     * Get current task status
     */
    public function status()
    {
        return $this->hasOne('App\TaskStatus', 'id', 'status_id');
    }
}