<?php

use Illuminate\Database\Seeder;

class TaskStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks_statuses')->insert([
            ['name' => 'Open'],
            ['name' => 'In Progress'],
            ['name' => 'In Review'],
            ['name' => 'Closed']
        ]);
    }
}
