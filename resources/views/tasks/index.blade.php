@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        All Tasks
                        <a href="{{ route('new_task') }}" class="btn btn-default">
                            <i class="fa fa-plus"></i> New
                        </a>
                    </div>
                    <div class="panel-body">
                        <table class="tasks-table">
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Master</th>
                                <th>Executor</th>
                                <th>Status</th>
                            </tr>
                            <div style="display: none">{{$i = 1}}</div>
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><a href="{{ route('view_task', ['id' => $task->id]) }}">{{ $task->title }}</a></td>
                                    <td>{{ $task->master->name }}</td>
                                    @if ($task->executor !== null)
                                        <td>{{ $task->executor->name }}</td>
                                    @else
                                        <td>--</td>
                                    @endif
                                    <td>{{ $task->status->name }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
